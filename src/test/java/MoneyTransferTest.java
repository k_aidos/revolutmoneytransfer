import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.async.Callback;
import com.mashape.unirest.http.exceptions.UnirestException;
import entities.Account;
import entities.Card;
import entities.Event;
import entities.Transfer;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.opentest4j.AssertionFailedError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.concurrent.Future;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class MoneyTransferTest {
    private static final Logger LOG = LoggerFactory.getLogger(MoneyTransferTest.class);
    private static final String UTF8 = "UTF-8";

    private static MoneyTransfer moneyTransfer;
    private static String baseUrl;

    @Before
    public void setUp() throws Exception {
        moneyTransfer = new MoneyTransfer();
        moneyTransfer.start();
        baseUrl = "http://localhost:8080";
        setupUnirest();
    }

    private static void setupUnirest() {
        Unirest.setObjectMapper(new ObjectMapper() {
            private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper
                    = new com.fasterxml.jackson.databind.ObjectMapper()
                    .registerModule(new JavaTimeModule())
                    .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        Unirest.setDefaultHeader("Accept", "application/json");
    }

//    @Test
//    public void getAccountsById() throws UnirestException, UnsupportedEncodingException {
//        int id = 1;
//        Account account = Unirest.get(baseUrl + "/accounts")
//                .queryString("id", id)
//                .asObject(Account.class).getBody();
//        assertNotNull(account);
//        assertEquals("Roman Abramovich", account.getName());
//    }

    @Test
    public void postEvent() throws UnirestException {
        String url = baseUrl + "/events";
        String sender = "1111222233334443";
        String receiver = "1111222233334444";
        BigDecimal amount = new BigDecimal("10.00");
        ZonedDateTime dateTime = ZonedDateTime.now(ZoneOffset.UTC);

        Transfer transfer = new Transfer(sender, receiver, amount, dateTime);
        String cardsUrl = baseUrl + "/cards?id={id}";
        Card roman = Unirest.get(cardsUrl)
                .routeParam("id", transfer.getSender())
                .asObject(Card.class).getBody();
        Card cfc = Unirest.get(cardsUrl)
                .routeParam("id", transfer.getReceiver())
                .asObject(Card.class).getBody();

        Future<HttpResponse<Event>> future = Unirest.post(url)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .body(transfer)
                .asObjectAsync(Event.class, new Callback<Event>() {

                    public void failed(UnirestException e) {
                        fail("Request failed: ", e);
                    }

                    public void completed(HttpResponse<Event> response) {
                        Event event = response.getBody();
                        assertNotNull(event);
                        assertEquals(transfer.getId(), event.getTransferId());
                        assertEquals(transfer.getSender(), event.getCardNumber());
                        assertEquals(0, event.getAmount().compareTo(transfer.getAmount().negate()));
                    }

                    public void cancelled() {
                        fail("Request cancelled");
                    }

                });
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            fail("Unexpected exception", e);
        }

        if (future.isDone()) {
            try {
                BigDecimal expected = roman.getBalance().add(transfer.getAmount().negate());
                BigDecimal actual = Unirest.get(cardsUrl)
                        .routeParam("id", transfer.getSender())
                        .asObject(Card.class).getBody().getBalance();
                Assertions.assertNotNull(actual);
                Assertions.assertEquals(0, expected.compareTo(actual));
            } catch (UnirestException| AssertionFailedError e) {
                fail("Balance check failed for sender: ", e);
            }

            try {
                BigDecimal expected = cfc.getBalance().add(transfer.getAmount());
                BigDecimal actual = Unirest.get(cardsUrl)
                        .routeParam("id", transfer.getReceiver())
                        .asObject(Card.class).getBody().getBalance();
                Assertions.assertNotNull(actual);
                Assertions.assertEquals(0, expected.compareTo(actual));
            } catch (UnirestException| AssertionFailedError e) {
                fail("Balance check failed for receiver: ", e);
            }
        } else {
            fail("Timeout");
        }
    }
}
