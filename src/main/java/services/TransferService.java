package services;

import dao.CardDao;
import dao.EventDao;
import dao.TransferDao;
import entities.Event;
import entities.Transfer;
import exceptions.CardNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.StampedLock;

public class TransferService implements TransferDao {

    private static final Logger LOG = LoggerFactory.getLogger(CardService.class);

    private CardDao cardDao;
    private EventDao eventDao;

    private ConcurrentHashMap<String, StampedLock> sendLocks;
    private ConcurrentHashMap<String, StampedLock> receiveLocks;
    private ExecutorService senders;
    private ExecutorService receivers;

    public TransferService(
            CardDao cardDao,
            EventDao eventDao,
            Integer sendersPoolSize,
            Integer receiversPoolSize) {
        this.cardDao = cardDao;
        this.eventDao = eventDao;
        this.senders = Executors.newFixedThreadPool(sendersPoolSize);
        this.receivers = Executors.newFixedThreadPool(receiversPoolSize);
        this.sendLocks = new ConcurrentHashMap<>(sendersPoolSize * 2);
        this.receiveLocks = new ConcurrentHashMap<>(receiversPoolSize * 2);
    }

    private long createEvent(
            Event event,
            StampedLock lock,
            long stamp
    ) {
        stamp = lock.tryConvertToWriteLock(stamp);
        if (stamp != 0) {
            try {
                eventDao.create(event);
            } finally {
                stamp = lock.tryConvertToOptimisticRead(stamp);
            }
        } else {
            LOG.error("OptimistLockException");
        }
        return stamp;
    }

    @Override
    public CompletableFuture<Event> send(Transfer transfer) {
        return CompletableFuture.supplyAsync(() ->{
            Event send = null;
            StampedLock stampedLock = sendLocks.computeIfAbsent(transfer.getId(), is -> new StampedLock());
            long stamp = stampedLock.tryOptimisticRead();
            try {
                cardDao.findById(transfer.getSender());
                    send = new Event(
                            transfer.getSender(),
                            transfer.getAmount().negate(),
                            ZonedDateTime.now(ZoneOffset.UTC),
                            transfer.getId()
                    );
                    stamp = createEvent(send, stampedLock, stamp);
                LOG.info("Outgoing transfer {}", transfer);
            } catch (Exception e) {
                LOG.error("Transfer failed: {}", transfer, e);
                e.printStackTrace();
            } finally {
                if (stampedLock.tryConvertToWriteLock(stamp) != 0) {
                    sendLocks.remove(transfer.getId());
                }
                receivers.submit(() -> receive(transfer));
            }
            return send;
        }, senders);
    }

    @Override
    public CompletableFuture<Event> receive(Transfer transfer) {
        return CompletableFuture.supplyAsync(() -> {
            Event receive = null;
            StampedLock stampedLock = receiveLocks.computeIfAbsent(transfer.getId(), is -> new StampedLock());
            long stamp = stampedLock.tryOptimisticRead();
            try {
                 cardDao.findById(transfer.getReceiver());
                    receive = new Event(
                            transfer.getReceiver(),
                            transfer.getAmount(),
                            ZonedDateTime.now(ZoneOffset.UTC),
                            transfer.getId()
                    );
                    stamp = createEvent(receive, stampedLock, stamp);
                LOG.info("Incoming transfer: {}", transfer);
            } catch (CardNotFoundException e) {
                Event refund = new Event(
                            transfer.getSender(),
                            transfer.getAmount(),
                            ZonedDateTime.now(ZoneOffset.UTC),
                            transfer.getId()
                    );
                    eventDao.create(refund);
                LOG.info("Transfer failed. Refunding money");
            } catch (Exception e) {
                LOG.error("Transfer failed: {}. Reason: ", transfer, e);
                e.printStackTrace();
            } finally {
                if (stampedLock.tryConvertToWriteLock(stamp) != 0) {
                    receiveLocks.remove(transfer.getId());
                }
            }
            return receive;
        }, receivers);
    }

}
