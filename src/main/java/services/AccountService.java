package services;

import dao.AccountDao;
import entities.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.DbUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class AccountService extends DbUtil implements AccountDao {

    private static final Logger LOG = LoggerFactory.getLogger(AccountService.class);
    private static final String SELECT_ACCOUNT_BY_ID = "SELECT * FROM ACCOUNT WHERE ID = ?";
    private static final String SELECT_ALL_ACCOUNTS = "SELECT * FROM ACCOUNT";

    private Connection connection = getConnection();
    private PreparedStatement preparedStatement = null;
    private Account account = new Account();

    @Override
    public Optional<Account> findById(int id) throws SQLException {
        try {
            preparedStatement = connection.prepareStatement(SELECT_ACCOUNT_BY_ID);
            preparedStatement.setInt(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.first()){
                account.setId(resultSet.getInt("id"));
                account.setName(resultSet.getString("name"));
                account.setAccountNumber(resultSet.getString("accountnumber"));
                }

        } catch (SQLException e) {
            LOG.error("Failed to find Account by ID");
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return Optional.of(account);
    }

    @Override
    public List<Account> findAll() throws SQLException {
        List<Account> list = new LinkedList<>();
        try {
            preparedStatement = connection.prepareStatement(SELECT_ALL_ACCOUNTS);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Account account = new Account();
                account.setId(resultSet.getInt("ID"));
                account.setName(resultSet.getString("NAME"));
                account.setAccountNumber(resultSet.getString("ACCOUNTNUMBER"));
                list.add(account);
            }
        } catch (SQLException e) {
            LOG.error("Failed to find ALL");
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return list;
    }

}
