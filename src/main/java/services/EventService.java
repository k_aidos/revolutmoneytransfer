package services;

import dao.EventDao;
import entities.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.DbUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;

public class EventService extends DbUtil implements EventDao {

    private static final Logger LOG = LoggerFactory.getLogger(EventService.class);
    private static final String UPDATE_BALANCE_BY_ID = "UPDATE CARD SET BALANCE = BALANCE + ? WHERE ID = ?";

    private Connection connection = getConnection();
    private PreparedStatement preparedStatement = null;

    @Override
    public void create(Event event) {
            try {
                preparedStatement = connection.prepareStatement(UPDATE_BALANCE_BY_ID);
                preparedStatement.setBigDecimal(1, event.getAmount());
                preparedStatement.setString(2, event.getCardNumber());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                LOG.error("Failed to create event");
                e.printStackTrace();
            } finally {
                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        LOG.error("Failed to close statement");
                        e.printStackTrace();
                    }
                }
            }
    }

}
