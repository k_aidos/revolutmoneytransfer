package services;

import dao.CardDao;
import entities.Account;
import entities.Card;
import exceptions.CardNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.DbUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class CardService extends DbUtil implements CardDao {

    private static final Logger LOG = LoggerFactory.getLogger(CardService.class);
    private static final String SELECT_CARD_BY_ID = "SELECT * FROM CARD WHERE ID = ?";

    private Connection connection = getConnection();
    private PreparedStatement preparedStatement = null;
    private Card card = new Card();
    private AccountService accountService = new AccountService();

    @Override
    public Optional<Card> findById(String id) throws SQLException {
        try {
            preparedStatement = connection.prepareStatement(SELECT_CARD_BY_ID);
            preparedStatement.setString(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.first()){
                card.setId(resultSet.getString("id"));
                Optional<Account> account = accountService.findById(resultSet.getInt("account_id"));
                account.ifPresent(value -> card.setAccount(value));
                card.setBalance(resultSet.getBigDecimal("balance"));
            } else {
                LOG.error("Card not found:"+ id);
                throw new CardNotFoundException(id);
            }
        } catch (SQLException e) {
            LOG.error("Failed to find Account by ID");
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return Optional.of(card);
    }

}
