package utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DbUtil {

    private static final Logger LOG = LoggerFactory.getLogger(DbUtil.class);

    public static final String DRIVER = "org.h2.Driver";
    public static final String URL = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
    public static final String SCRIPTS = "money_transfer.sql";
    private Connection connection = null;
    private Statement statement = null;

    public Connection getConnection(){
        try {
            Class.forName(DRIVER);
            connection = DriverManager.getConnection(URL);
        } catch (ClassNotFoundException | SQLException e) {
            LOG.error("Failed to connect");
            e.printStackTrace();
        }
        return connection;
    }

    public void executeScripts() throws SQLException {
        try {
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection(URL);
            statement = connection.createStatement();
            statement.execute(readResourceAsString());
        } catch (ClassNotFoundException | SQLException e) {
            LOG.error("Failed to execute scripts");
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    static String readResourceAsString() {
        String result;
        try(InputStream in = DbUtil.class.getClassLoader().getResourceAsStream(SCRIPTS)) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length;
            while ((length = in.read(buffer)) != -1) {
                out.write(buffer, 0, length);
            }
            result = out.toString(StandardCharsets.UTF_8.name());
        } catch (IOException e) {
            LOG.error("Failed to read resource '{}'", SCRIPTS, e);
            throw new RuntimeException(e);
        }
        return result;
    }
}
