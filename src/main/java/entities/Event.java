package entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;

public final class Event {

    private static final String HASH_TEMPLATE = "{ \"cardNumber\": \"%s\", \"amount\": %s, \"dateTime\": %d, \"transferId\": \"%s\" }";

    private String id;
    private final String cardNumber;
    private final BigDecimal amount;
    private final ZonedDateTime dateTime;
    private final String transferId;
    @JsonCreator
    public Event(@JsonProperty("cardNumber") String cardNumber,
                 @JsonProperty("amount") BigDecimal amount,
                 @JsonProperty("dateTime") ZonedDateTime dateTime,
                 @JsonProperty("transferId") String transferId) {
        this.cardNumber = cardNumber;
        this.amount = amount;
        this.dateTime = dateTime;
        this.transferId = transferId;
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-1");
            md.update(String.format(
                    HASH_TEMPLATE,
                    this.cardNumber,
                    this.amount,
                    this.dateTime.toEpochSecond(),
                    this.transferId).getBytes(StandardCharsets.UTF_8));
            id = DigestUtils.sha1Hex(md.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public ZonedDateTime getDateTime() {
        return dateTime;
    }

    public String getTransferId() {
        return transferId;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id='" + id + '\'' +
                ", cardNumber='" + cardNumber + '\'' +
                ", amount=" + amount +
                ", dateTime=" + dateTime +
                ", transferId='" + transferId + '\'' +
                '}';
    }
}
