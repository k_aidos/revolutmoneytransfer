package entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.codec.digest.DigestUtils;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;

public class Transfer {

    private static final String HASH_TEMPLATE = "{ \"sender\": \"%b\", \"receiver\": \"%s\", \"amount\": %s, \"dateTime\": %d }";

    private String id;
    private final String sender;
    private final String receiver;
    private final BigDecimal amount;
    private final ZonedDateTime dateTime;
    @JsonCreator
    public Transfer(@JsonProperty("sender") String sender,
                    @JsonProperty("receiver") String receiver,
                    @JsonProperty("amount") BigDecimal amount,
                    @JsonProperty("dateTime") ZonedDateTime dateTime) {
        this.sender = sender;
        this.receiver = receiver;
        this.amount = amount;
        this.dateTime = dateTime;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(String.format(
                    HASH_TEMPLATE,
                    sender,
                    receiver,
                    amount.toPlainString(),
                    dateTime.toEpochSecond()
            ).getBytes(StandardCharsets.UTF_8));
            id = DigestUtils.sha1Hex(md.digest());
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e);
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public ZonedDateTime getDateTime() {
        return dateTime;
    }

    @Override
    public String toString() {
        return "Transfer{" +
                "id='" + id + '\'' +
                ", sender='" + sender + '\'' +
                ", receiver='" + receiver + '\'' +
                ", amount=" + amount +
                ", dateTime=" + dateTime +
                '}';
    }

}