package entities;

import java.math.BigDecimal;

public class Card {

    private String id;
    private BigDecimal balance;
    private Account account;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "Card{" +
                "id='" + id + '\'' +
                ", balance=" + balance +
                ", account=" + account +
                '}';
    }
}
