package dao;

import entities.Account;
import entities.Card;

import java.sql.SQLException;
import java.util.Optional;

public interface CardDao {

    Optional<Card> findById(String id) throws SQLException;
}
