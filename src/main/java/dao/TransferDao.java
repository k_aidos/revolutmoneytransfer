package dao;

import entities.Event;
import entities.Transfer;

import java.util.concurrent.CompletableFuture;

public interface TransferDao {

    CompletableFuture<Event> send(Transfer transfer);
    CompletableFuture<Event> receive(Transfer transfer);

}
