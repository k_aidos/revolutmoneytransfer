package dao;

import entities.Account;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface AccountDao {

    Optional<Account> findById(int id) throws SQLException;
    List<Account> findAll() throws SQLException;

}
