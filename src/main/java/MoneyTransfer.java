import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import dao.AccountDao;
import entities.Account;
import entities.Card;
import entities.Event;
import entities.Transfer;
import io.javalin.Javalin;
import io.javalin.plugin.json.JavalinJackson;
import io.javalin.plugin.json.JavalinJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import services.AccountService;
import services.CardService;
import services.EventService;
import services.TransferService;
import utils.DbUtil;

import java.sql.SQLException;
import java.util.Objects;
import java.util.Optional;

import static io.javalin.apibuilder.ApiBuilder.*;

public class MoneyTransfer {

    private static final Logger LOG = LoggerFactory.getLogger(MoneyTransfer.class);

    public static void main(String[] args) {
        MoneyTransfer moneyTransfer = new MoneyTransfer();
        try {
            moneyTransfer.start();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    void start() throws SQLException {
        dbSetup();
        JavalinJackson.configure(new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        );

        AccountService accountService = new AccountService();
        CardService cardService = new CardService();
        EventService eventService = new EventService();
        TransferService transferService = new TransferService(
                cardService,
                eventService,
                Integer.parseInt("2"),
                Integer.parseInt("2")
        );

        Javalin app = Javalin.create().start(8080);
        app.config.defaultContentType = "application/json";
        rest(accountService, cardService, transferService, app);

    }

    private void dbSetup() throws SQLException {
        DbUtil dbUtil = new DbUtil();
        dbUtil.getConnection();
        dbUtil.executeScripts();
    }

    private void rest(AccountService accountService, CardService cardService, TransferService transferService, Javalin app) {
        app.get("accounts/all", ctx -> ctx.json(accountService.findAll()));

        app.routes(() -> path("accounts", () -> get(ctx -> {
            String id = ctx.queryParam("id");
            Optional<Account> account = accountService.findById(Integer.parseInt(Objects.requireNonNull(id)));
            if (account.isPresent()) {
                ctx.json(account.get());
            } else  {
                ctx.result("Failed to find account");
            }
        })));

        app.routes(() -> path("cards", () -> get(ctx -> {
            String id = ctx.queryParam("id");
            Optional<Card> card = cardService.findById(id);
            if (card.isPresent()) {
                ctx.json(card.get());
            } else  {
                ctx.result("Failed to find card");
            }
        })));

        app.routes(() -> path("events", () -> post(ctx -> {
            String a = ctx.body();
            JavalinJson.fromJson(ctx.body(), Transfer.class);

            Transfer transfer = ctx.bodyAsClass(Transfer.class);
            String action = ctx.queryParam("action", "");
            assert action != null;
            switch (action) {
                case "send": ctx.result(transferService.send(transfer).thenApplyAsync(JavalinJackson.INSTANCE::toJson));
                    break;
                case "receive": ctx.result(transferService.receive(transfer).thenApplyAsync(JavalinJackson.INSTANCE::toJson));
                    break;
                default: ctx.result(transferService.send(transfer).thenApplyAsync(JavalinJackson.INSTANCE::toJson));
                    break;
            }
        })));
    }

}