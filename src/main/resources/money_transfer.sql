create table IF NOT EXISTS Account (
    id bigint,
    name varchar(255),
    accountNumber varchar(19),
    constraint ACCOUNT_PK primary key (id)
    );

create table IF NOT EXISTS Card (
    id varchar (19),
    balance number (18, 2),
    account_id bigint,
    constraint CARD_PK primary key (id),
    constraint CARD_FK foreign key  (account_id) references  Account(id)
    );

create table IF NOT EXISTS Transfer (
    id bigint,
    name varchar(255),
    accountNumber varchar(19),
    balance number (18, 2)
    );

insert into Account(id, name, accountnumber) values (1, 'Roman Abramovich', '1111111111111111');
insert into Account(id, name, accountnumber) values (2, 'Chelsea FC', '2222222222222222');

insert into Card(id, balance, account_id) values ('1111222233334443', 100, 1);
insert into Card(id, balance, account_id) values ('1111222233334444', 100, 2);